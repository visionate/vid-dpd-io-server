"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Resource = require("deployd/lib/resource.js");
const URL = require("url");
const Logger_1 = require("./com/visionate/logging/Logger");
const Utils_1 = require("./com/visionate/utils/Utils");
const env = (process.server && process.server.options && process.server.options.env) || null;
let publicDir = '/../../../public';
class IOServer extends Resource {
    constructor(options) {
        super(options);
        this.logger = Logger_1.CLog.createClogger('IOServer');
        this.clientGeneration = true;
        Resource.apply(this, arguments);
        this.config = {
            proto: this.config.proto || 'https:',
            host: this.config.host || 'localhost',
            port: this.config.port || 2800
        };
        this.logger.info('config:  %O', this.config);
        this.io = require('socket.io-client');
        this.url = URL.format({
            protocol: this.config.proto,
            hostname: this.config.host,
            port: this.config.port,
            slashes: true
        });
    }
    /**
     * Handle requests
     * @param ctx
     * @param next
     */
    handle(ctx, next) {
        if (ctx.method === 'GET') {
            if (ctx.query) {
                if (ctx.query.clientEvent) {
                    const message = ctx.query.clientEvent;
                    // this.logger.info( 'handle get: clientEvent  %o', message );
                    /* const event = {
                     event: 'syncEvent',
                     destination: device.ip,
                     targetProtocols: [ TargetProtocol.TCP ],
                     value: {
                     type: 'string',
                     data: this.projectDataService.currentProject.storageFolder  } };*/
                    let socketEvent;
                    try {
                        if (typeof message === 'string') {
                            socketEvent = JSON.parse(Utils_1.escapeJSON(message));
                        }
                        else {
                            socketEvent = message;
                        }
                    }
                    catch (e) {
                        this.logger.info('handle get JSON Parse error: %s: %s, %O', e.name, e.message, e);
                        return ctx.done(e);
                    }
                    this.logger.info('handle get: socketEvent  %o', socketEvent);
                    if (socketEvent.targetProtocols && Array.isArray(socketEvent.targetProtocols)) {
                        const socket = this.io(this.url, { autoConnect: true });
                        this.logger.info('socket with URL: %s', this.url);
                        socket.on('connect', () => {
                            this.logger.info('socket on connect');
                            this.logger.info('send socket.emit()');
                            socket.emit('clientEvent', socketEvent);
                            setTimeout(() => {
                                this.logger.info('send socket.close()');
                                socket.close();
                            }, 200);
                        });
                        socket.on('disconnect', () => {
                            this.logger.info('socket on disconnect');
                            return ctx.done(null, socketEvent);
                        });
                        socket.on('error', (error) => {
                            this.logger.error('socket on error: %o', error);
                            return ctx.done(error);
                        });
                    }
                    else {
                        this.logger.info('handle get: message is not valid  %o', message);
                        return ctx.done(null, socketEvent);
                    }
                }
                return ctx.done(null, { query: ctx.query });
            }
            return ctx.done(null, '');
        }
        else {
            var parts = ctx.url.split('/').filter(function (p) {
                return p;
            });
            var result = {};
            var domain = {
                url: ctx.url,
                parts: parts,
                query: ctx.query || '',
                body: ctx.body || '',
                'this': result,
                getHeader: function (name) {
                    if (ctx.req.headers && typeof name == 'string' && name) {
                        return ctx.req.headers[name.toLowerCase()];
                    }
                },
                setHeader: function (name, value) {
                    if (ctx.res.setHeader) {
                        ctx.res.setHeader(name, value);
                    }
                },
                setStatusCode: function (statusCode) {
                    if (typeof statusCode !== 'number')
                        throw new TypeError('Status code must be a number');
                    ctx.res.statusCode = statusCode;
                },
                setResult: function (val) {
                    if (typeof val === 'string' || typeof val === 'object') {
                        result = val;
                    }
                    else {
                        result = '' + val;
                    }
                }
            };
            if (ctx.method === 'POST' && this.events.post) {
                const logger = this.logger;
                this.events.post.run(ctx, domain, function (err) {
                    if (err) {
                        logger.error('run POST event: ', err);
                    }
                    ctx.done(err, result);
                });
            }
            else {
                next();
            }
        }
    }
}
IOServer.label = 'IO-Server';
IOServer.events = ['post'];
IOServer.clientGeneration = true;
IOServer.basicDashboard = {
    settings: [
        {
            name: 'proto',
            type: 'text',
            description: 'the socket-io-server protocol'
        }, {
            name: 'host',
            type: 'text',
            description: 'the socket-io-server host'
        }, {
            name: 'port',
            type: 'number',
            description: 'the socket-io-server port'
        }
    ]
};
/**
 * Module export
 */
module.exports = IOServer;
