# Readme

## USERDIR/id_rsa.pub in remote USERDIR/.ssh/authorized_keys einfügen

    cat ~/.ssh/id_rsa.pub | ssh admin@10.0.10.197 "cat >> ~/.ssh/authorized_keys"

## SSH Login

    10.0.10.197
    admin
    eg%G8y7g
   
    # normal login
    ssh admin@10.0.10.197
    
    # ssh key login
    ssh -i ~/.ssh/id_rsa admin@10.0.10.197
    
    
## log into docker container

    # become root
    sudo su
    
    # log into container
    docker exec -t -i GEC-SERVER-API /bin/bash
    
    # change to project root
    cd /app/src/apiApp/
    
    # logs
    npm run logs
    
    # install
    npm install vid-dpd-io-server

