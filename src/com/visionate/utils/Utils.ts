export const escapeJSON = ( json ) => {
  return json
    .replace( /[\b]/g, '\\b' )
    .replace( /[\f]/g, '' )
    .replace( /[\n]/g, '' )
    .replace( /[\r]/g, '' )
    .replace( /[\t]/g, '' );
};

export function hook_stream( stream, callback ) {
  let old_write = stream.write;

  stream.write = (function ( write ) {
    return function ( string, encoding, fd ) {
      write.apply( stream, arguments );  // comments this line if you don't want output in the console
      callback( string, encoding, fd );
    };
  })( stream.write );

  return function () {
    stream.write = old_write;
  };
}
