import * as Resource from 'deployd/lib/resource.js';
import { clone } from 'lodash';
import * as URL from 'url';
import { CLog, CLogger } from './com/visionate/logging/Logger';
import { SocketEvent } from './com/visionate/model/Socket-Event';
import { escapeJSON } from './com/visionate/utils/Utils';

const env = ((process as any).server && (process as any).server.options && (process as any).server.options.env) || null;

let publicDir = '/../../../public';

class IOServer extends Resource {

  public logger: CLogger = CLog.createClogger( 'IOServer' );

  public static label = 'IO-Server';

  public static events = [ 'post' ];

  public events;

  public static clientGeneration = true;

  public static basicDashboard = {
    settings: [
      {
        name: 'proto',
        type: 'text',
        description: 'the socket-io-server protocol'
      }, {
        name: 'host',
        type: 'text',
        description: 'the socket-io-server host'
      }, {
        name: 'port',
        type: 'number',
        description: 'the socket-io-server port'
      }
    ]
  };

  public clientGeneration = true;

  public config;

  public name;

  public io;

  public url;

  constructor( options ) {
    super( options );

    Resource.apply( this, arguments );

    this.config = {
      proto: this.config.proto || 'https:',
      host: this.config.host || 'localhost',
      port: this.config.port || 2800
    };

    this.logger.info( 'config:  %O', this.config );

    this.io = require( 'socket.io-client' );
    this.url = URL.format( {
      protocol: this.config.proto,
      hostname: this.config.host,
      port: this.config.port,
      slashes: true
    } );
  }

  /**
   * Handle requests
   * @param ctx
   * @param next
   */
  public handle( ctx, next ) {

    if ( ctx.method === 'GET' ) {
      if ( ctx.query ) {

        if ( ctx.query.clientEvent ) {

          const message = ctx.query.clientEvent;
          // this.logger.info( 'handle get: clientEvent  %o', message );

          /* const event = {
           event: 'syncEvent',
           destination: device.ip,
           targetProtocols: [ TargetProtocol.TCP ],
           value: {
           type: 'string',
           data: this.projectDataService.currentProject.storageFolder  } };*/

          let socketEvent: SocketEvent;
          try {
            if ( typeof message === 'string' ) {
              socketEvent = JSON.parse( escapeJSON( message ) );
            } else {
              socketEvent = message;
            }

          } catch ( e ) {
            this.logger.info( 'handle get JSON Parse error: %s: %s, %O', e.name, e.message, e );
            return ctx.done( e );
          }
          this.logger.info( 'handle get: socketEvent  %o', socketEvent );

          if ( socketEvent.targetProtocols && Array.isArray( socketEvent.targetProtocols ) ) {

            const socket = this.io( this.url, { autoConnect: true } );
            this.logger.info( 'socket with URL: %s', this.url );

            socket.on( 'connect', () => {
              this.logger.info( 'socket on connect' );
              this.logger.info( 'send socket.emit()' );
              socket.emit( 'clientEvent', socketEvent );

              setTimeout( () => {
                this.logger.info( 'send socket.close()' );
                socket.close();
              }, 200 );
            } );

            socket.on( 'disconnect', () => {
              this.logger.info( 'socket on disconnect' );
              return ctx.done( null, socketEvent );
            } );
            socket.on( 'error', ( error ) => {
              this.logger.error( 'socket on error: %o', error );
              return ctx.done( error );
            } );

          } else {
            this.logger.info( 'handle get: message is not valid  %o', message );
            return ctx.done( null, socketEvent );
          }
        }
        return ctx.done( null, { query: ctx.query } );
      }
      return ctx.done( null, '' );

    } else {

      var parts = ctx.url.split( '/' ).filter( function ( p ) {
        return p;
      } );

      var result = {};

      var domain = {
        url: ctx.url,
        parts: parts,
        query: ctx.query || '',
        body: ctx.body || '',
        'this': result,
        getHeader: function ( name ) {
          if ( ctx.req.headers && typeof name == 'string' && name ) {
            return ctx.req.headers[ name.toLowerCase() ];
          }
        },
        setHeader: function ( name, value ) {
          if ( ctx.res.setHeader ) {
            ctx.res.setHeader( name, value );
          }
        },
        setStatusCode: function ( statusCode ) {
          if ( typeof statusCode !== 'number' ) throw new TypeError( 'Status code must be a number' );
          ctx.res.statusCode = statusCode;
        },
        setResult: function ( val ) {
          if ( typeof val === 'string' || typeof val === 'object' ) {
            result = val;
          } else {
            result = '' + val;
          }
        }
      };

      if ( ctx.method === 'POST' && this.events.post ) {
        const logger = this.logger;
        this.events.post.run( ctx, domain, function ( err ) {
          if ( err ) {
            logger.error( 'run POST event: ', err );
          }
          ctx.done( err, result );
        } );
      } else {
        next();
      }
    }
  }
}

/**
 * Module export
 */
module.exports = IOServer;
