"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SocketEvent {
    constructor() {
        this.event = '';
        this.senderAddress = undefined;
        this.destination = undefined;
        // get | post | put | delete
        this.method = undefined;
        this.targetProtocols = undefined;
        this.value = undefined;
    }
}
exports.SocketEvent = SocketEvent;
