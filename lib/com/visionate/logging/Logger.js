"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
if (!process.env.LOGNAME) {
    throw new Error('Please set process.env.LOGNAME !!!');
}
var debug = require('debug');
var error = debug(process.env.LOGNAME + ':error:vid-dpd-io-server');
var warn = debug(process.env.LOGNAME + ':warn:vid-dpd-io-server');
warn.log = console.warn.bind(console);
var log = debug(process.env.LOGNAME + ':log:vid-dpd-io-server');
log.log = console.info.bind(console);
var trace = debug(process.env.LOGNAME + ':trace:vid-dpd-io-server');
trace.log = console.debug.bind(console);
class CLog {
    static createClogger(name) {
        return new CLogger(name);
    }
}
exports.CLog = CLog;
class CLogger {
    constructor(name) {
        this.errorLogger = error;
        this.logger = log;
        this.warner = warn;
        this.tracer = trace;
    }
    trace(...otherParams) {
        this.tracer(...otherParams);
        console.debug(otherParams);
    }
    log(...otherParams) {
        this.logger(...otherParams);
        console.log(otherParams);
    }
    info(...otherParams) {
        this.logger(...otherParams);
        console.info(otherParams);
    }
    warn(...otherParams) {
        this.warner(...otherParams);
        console.warn(otherParams);
    }
    error(...otherParams) {
        this.errorLogger(...otherParams);
        console.error(otherParams);
    }
}
exports.CLogger = CLogger;
