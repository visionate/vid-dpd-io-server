"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IOTypes;
(function (IOTypes) {
    IOTypes["TCPSocketServer"] = "TCPSocketServer";
    IOTypes["TCPSocketClient"] = "TCPSocketClient";
    IOTypes["SocketIOServer"] = "SocketIOServer";
    IOTypes["KNXConnector"] = "KNXConnector";
    IOTypes["OSCServer"] = "OSCServer";
    IOTypes["DPDRequest"] = "DPDRequest";
})(IOTypes = exports.IOTypes || (exports.IOTypes = {}));
var TargetProtocol;
(function (TargetProtocol) {
    TargetProtocol["KNX"] = "KNX";
    TargetProtocol["TCP"] = "TCP";
    TargetProtocol["IO"] = "IO";
    TargetProtocol["DPD"] = "DPD";
    TargetProtocol["OSC"] = "OSC";
})(TargetProtocol = exports.TargetProtocol || (exports.TargetProtocol = {}));
class IOObject {
    constructor() {
        /**
         * @default ''
         */
        this.id = '';
        /**
         * @default false
         */
        this.enabled = false;
        /**
         * @default ''
         */
        this.IOType = '';
        this.protocol = undefined;
        this.targetIDs = undefined;
        this.targetProtocols = undefined;
        this.config = undefined;
    }
}
exports.IOObject = IOObject;
