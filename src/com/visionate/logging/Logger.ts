if ( !process.env.LOGNAME ) {
  throw new Error( 'Please set process.env.LOGNAME !!!' );
}
var debug = require( 'debug' );

var error = debug(  process.env.LOGNAME + ':error:vid-dpd-io-server' );

var warn = debug( process.env.LOGNAME + ':warn:vid-dpd-io-server' );
warn.log = console.warn.bind( console );

var log = debug( process.env.LOGNAME + ':log:vid-dpd-io-server' );
log.log = console.info.bind( console );

var trace = debug( process.env.LOGNAME + ':trace:vid-dpd-io-server' );
trace.log = console.debug.bind( console );

export class CLog {
  static createClogger<TA>( name: string ): CLogger {
    return new CLogger( name );
  }
}

export class CLogger {
  private logger;

  private warner;

  private tracer;

  private errorLogger;

  constructor( name ) {
    this.errorLogger = error;
    this.logger = log;
    this.warner = warn;
    this.tracer = trace;
  }

  public trace( ...otherParams: any[] ): void {
    this.tracer( ...otherParams );
    console.debug(otherParams);
  }

  public log( ...otherParams: any[] ): void {
    this.logger( ...otherParams );
    console.log(otherParams);
  }

  public info( ...otherParams: any[] ): void {
    this.logger( ...otherParams );
    console.info(otherParams);
  }

  public warn( ...otherParams: any[] ): void {
    this.warner( ...otherParams );
    console.warn(otherParams);
  }

  public error( ...otherParams: any[] ): void {
    this.errorLogger( ...otherParams );
    console.error(otherParams);
  }
}
